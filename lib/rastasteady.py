#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import math
import subprocess
import os
import re
from pyfiglet import Figlet
import time
from subprocess import check_output

class RastaSteady:
   def __init__(self, filename = ''):
      self.filename = filename
      self.getresolution()

   def derp_it(self, tx, target_width, src_width):
      x = (float(tx) / target_width - 0.5) * 2
      sx = tx - (target_width - src_width) / 2
      offset = math.pow(x, 2) * (-1 if x < 0 else 1) * ((target_width - src_width) / 2)
      return sx - offset
   
   def getresolution(self):
      out = subprocess.Popen(['ffprobe', '-v', 'error','-show_entries','stream=width,height','-of','csv=p=0:s=x', self.filename],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT) ## if STDERROR !!!
      stdout, stderr = out.communicate()
      cleanmsg = stdout.decode('utf-8')
      listamedida = cleanmsg.split('x')
      self.src_width = int(listamedida[0].strip('\n'))
      height = listamedida[1].strip('\n')
      self.height = int(height)
               
   def dual(self):
      command = 'ffmpeg -i ' + self.filename + ' -i rastaview-'+ self.filename + ' -filter_complex hstack dual-' + self.filename
      os.system(command)

   def stabilize(self):
      command0 = 'cp ' + self.filename + ' ' + 'original-' + self.filename
      os.system(command0)
      command1 = 'ffmpeg -i '+ self.filename + ' -vf vidstabdetect -f null -'
      os.system(command1)
      command2 = 'ffmpeg -i '+ self.filename + ' -vf vidstabtransform=zoom=-5:input="transforms.trf" ' + 'stabilized-' + self.filename
      os.system(command2)

   def rastaview(self, percentage = 20):
      if percentage == 0:
         return

      target_width = self.src_width + (self.src_width/100*int(percentage))
      target_width = int(target_width)

      xmap = open('xmap.pgm', 'w')
      xmap.write('P2 {0} {1} 65535\n'.format(target_width, self.height))
      for y in range(self.height):
         for x in range(target_width):
            fudgeit = self.derp_it(x, target_width, self.src_width)
            xmap.write('{0} '.format(int(fudgeit)))
         xmap.write('\n')
      xmap.close()

      ymap = open('ymap.pgm', 'w')
      ymap.write('P2 {0} {1} 65535\n'.format(target_width, self.height))
      for y in range(self.height):
         for x in range(target_width):
            ymap.write('{0} '.format(y))
         ymap.write('\n')
      ymap.close()

      command = 'ffmpeg -i stabilized-' + self.filename + ' -i xmap.pgm -i ymap.pgm -lavfi remap rastaview-' + self.filename
      os.system(command)

#myVideo = RastaSteady(filename = 'test.mp4')
#myVideo.stabilize()
#myVideo.rastaview(percentage = 20)
#myVideo.dual()
