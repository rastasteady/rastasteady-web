from setuptools import setup

setup(
    name='RastaSteady Web',
    version='0.1',
    py_modules=['rastasteady-web'],
    install_requires=[
        'Flask',
    ],
    entry_points='''
        [console_scripts]
        rastasteady-web=web:web
    ''',
)
