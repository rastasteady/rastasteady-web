# RastaSteady Web
RastaSteady es un software de estabilizacion de video para el sistema DJI FPV digital.

### Instalacion
#### Genérica
RastaSteady Web se puede instalar desde [PyPi](https://pypi.org/):
```sh
$ pip install --user rastasteady-web

```

#### Desde el código fuente
Para usar el código fuente tal cual se publica se require [pipenv](https://pypi.org/project/pipenv/):
```sh
$ cd rastasteady-web
$ pipenv install
$ pipenv shell
$ pip install --editable .
```
#### Contenedor
RastaSteady se puede ejecutar desde un contenedor para evitar conflictos de dependencias en el equipo donde se quiere ejecutar.

```sh
$ alias rastasteady-web="docker run -v $PWD:/workdir quay.io/rastasteady/rastasteady-web"
```

### Por hacer
#### Visual
 - desacoplar el proceso de carga de fichero con el procesado (webservice que lo haga?)
 - crear formulario de proceso para indicar resolucion del fichero final asi como el porcentaje de Rastaview
 - aplicar hoja de estilos

#### Usabilidad de la clase RastaSteady
 - agregar opciones que permitan especificar la resolucion del fichero final deseado
 - poder trabajar con ficheros de entrada en directorios distintos al actual
 - poder definir el nombre y localización de los ficheros finales

#### Varios
 - crear dependencia de la libreria rastasteady cuando esté publicada en [PyPi](https://pypi.org/).
